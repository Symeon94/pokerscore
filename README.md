
# Poker Score

### What is it?

The poker score is a small Java program written to be able to calculate a classement for different poker player over the course of a year and a half through multiple different poker game.

### How does it work?

The program is a simple Java program. You have to put all the score in the following format in a `score.txt` file.
```
MM/DD/YYYY
Player-1st-Place
Player-2nd-Place
Player-3rd-Place
...
Player-nth-Place
```
Then simply compile and run the program
```
javac ScoreCalculator.java
java ScoreCalculator
```
A `score.csv` UTF-8 format file will be generated containing the result of the different players for each game and updated. To understand more how to score is generated look at the `score.pdf` file which will contain every mathematical explanation about it.

### Example

With the score file uploaded on the bitbucket repository, if you run the program, you'll get the following result in a the output file.

| Player | 12/03/2015 | 12/04/2015 | 12/09/2015 | 25/11/2015 | Updated |
|:------ | ----------:| ----------:| ----------:| ----------:| -------:|
|Marc|50|22|65|158|244
|Jean|112|88|48|35|184
|Luc|0|138|49|61|182
|Bernard|12|5|146|22|157
|Paul|0|50|36|88|145
|Matthieu|0|0|0|120|114
|Jean-Marc|0|0|102|2|90
|Ginette|0|0|4|39|40
|Charles|0|0|16|26|39
|Josette|0|0|0|9|9
|George|0|0|0|0|0



### Upgrade

I will try to create  a GUI version of the program for easier use for non-IT persons. It will come probably in the future.

----------------------------------------

# Poker Score

### De quoi s'agit-il?

Poker score est un petit programme en Java ecrit pour etre capable de calculer un classement pour different joueurs de poker sur le cours d'une annee et demis de differentes parties de poker.

### Comment cela fonctionne?

Le programme est un programme simple en Java. Vous devez mettre tout les scores avec le format suivant dans un fichier `score.txt`.

```
MM/JJ/AAAA
Joueur-1ere-Place
Joueur-2eme-Place
Joueur-3eme-Place
...
Joueur-neme-Place
```

Ensuite compilez et lancez le programme Java.

```
javac ScoreCalculator.java
java ScoreCalculator
```

Un fichier `score.csv` en format UTF-8 va etre genere contenant le resultat pour les differents joueurs pour chaque partie et mis a jour a la date d'aujourd'hui. Pour mieux comprendre comment est genere le score, regardez dans le fichier `score.pdf` qui contient l'explication mathematique pour le calcul du score.

### Exemple

Avec le fichier de score disponible sur le repository bitbucket, si vous utilisez le programme, vous obtiendrez le resultat suivant dans le fichier de sortie.

| Player | 12/03/2015 | 12/04/2015 | 12/09/2015 | 25/11/2015 | Updated |
|:------ | ----------:| ----------:| ----------:| ----------:| -------:|
|Marc|50|22|65|158|244
|Jean|112|88|48|35|184
|Luc|0|138|49|61|182
|Bernard|12|5|146|22|157
|Paul|0|50|36|88|145
|Matthieu|0|0|0|120|114
|Jean-Marc|0|0|102|2|90
|Ginette|0|0|4|39|40
|Charles|0|0|16|26|39
|Josette|0|0|0|9|9
|George|0|0|0|0|0


### Ameliorations

Je vais essayer d'ecrire un programme avec interface graphique pour une utilisation plus simple pour les personnes qui ne sont pas expert en informatique.
