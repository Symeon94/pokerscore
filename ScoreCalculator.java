import java.io.*;
import java.util.*;

public class ScoreCalculator
{
	public static ArrayList<String> players;
	public static LinkedList<Game> games;
	
	public static void main(String[] args)	{
		players = new ArrayList<String>();;
		games = new LinkedList<Game>();
		
		BufferedReader file = null;
		try {
			file = new BufferedReader(new FileReader(new File("score.txt")));
			String data = null;
			while((data = file.readLine()) != null) {
				if(data.length() > 0) {
					Game g = new Game(data);
					games.add(g);
					loadGame(g, file);
					g.calculateScore();
				}
			}
			file.close();			
		}
		catch(IOException ioe) {
			ioe.printStackTrace();
		}
		
		// Sort the games
		Collections.sort(games);
		
		// Calculate the score for each games and output it
		BufferedWriter writer = null;
		try{
			writer = new BufferedWriter(new FileWriter(new File("score.csv")));
			// Write header
			writer.append("Player");
			for(Game g : games) {
				writer.append(String.format(";%02d/%02d/%d", g.d.getDate() ,g.d.getMonth()+1, 1900+g.d.getYear()));
			}
			Date today = new Date();
			writer.append(String.format(";Updated", today.getDate() ,today.getMonth()+1, 1900+today.getYear()));
			writer.newLine();
			
			// Calculate all scores
			Game previous = null;
			for(Game g : games) {
				g.calculateScore(previous, players);
				previous = g;
			}
			
			
			
			// Print games data
			for(String p : players) {
				writer.append(p);
				for(Game g : games) {
					float tmp = g.score.get(p);
					writer.append(String.format(";%d", (int)tmp));
				}
				float tmp = 0;
				for(Game g : games) {
					tmp += g.getUpdatedScore(p, today);
				}
				writer.append(String.format(";%d", (int)tmp));
				writer.newLine();
			}
			
			writer.close();
		}
		catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	private static void loadGame(Game g, BufferedReader file) throws IOException {
		String data = null;
		while((data = file.readLine()) != null) {
			if(data.length() == 0)
				return;
			g.players.add(data);
			if(!players.contains(data))
				players.add(data);
		}
	}
}

class Game implements Comparable<Game> {
	public ArrayList<String> players;
	public Date d;
	public HashMap<String, Float> score;
	
	public Game(String date) {
		this.d = new Date(date);
		players = new ArrayList<String>();
		score = new HashMap<String, Float>();
	}
	
	public void calculateScore(Game previous, ArrayList<String> allPlayers) {
		// No previous
		if(previous == null) {
			for(String s : allPlayers) {
				if(score.get(s) == null)
					score.put(s, 0.f);
			}
		}
		else {
			for(String s : allPlayers) {
				// Player is not from this game we calculate the score
				if(score.get(s) == null) {
					float scoreRes = 0;
					float n = (float)players.size();
					for(String playerInGame: players) {
						float denom = (previous.getUpdatedScore(s, d)+previous.getUpdatedScore(playerInGame, d));
						if(denom != 0) {
							float chanceyx = previous.getUpdatedScore(s, d)/(previous.getUpdatedScore(s, d)+previous.getUpdatedScore(playerInGame, d));
							float st1 = score.get(playerInGame);
							scoreRes += (chanceyx*st1)/n;
						}
					}
					score.put(s, scoreRes);
				}
			}
		}
	}
	
	public float getUpdatedScore(String player, Date date) {
		Float s = score.get(player);
		if(s == null)
			return 0;
		long diff = (date.getTime() - d.getTime())/(1000*60*60*24);
		float multiplier = (float)Math.max(0, (1 - Math.pow(diff/548.0,2)));
		return s * multiplier;
	}
	
	public void calculateScore() {
		int i = 1;
		for(String p : players) {
			score.put(p, Game.score(players.size(), i));
			i++;
		}
	}
	
	private static float score(int n, int i) {
		if(n == 0)
			return 0.f;
		return 200.f*(float)Math.pow(n - i,2)/(float)Math.pow(n,2);
	}
	
	public int compareTo(Game g) {
		return this.d.compareTo(g.d);
	}
	
	public boolean equals(Object o) {
		if(o instanceof Game) {
			Game tmp = (Game)o;
			return tmp.d.equals(this.d);
		}
		return false;
	}
	
	public String toString(){		
		return d.toString();
	}
}