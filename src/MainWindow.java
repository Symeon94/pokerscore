import javax.swing.*;

public class MainWindow extends JFrame{
  public static final String APP_NAME = "Poker Score";

  public MainWindow() {
    super(APP_NAME);
    this.pack();
    this.setVisible(true);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  public static void main(String[] args) {
    new MainWindow();
  }
}
